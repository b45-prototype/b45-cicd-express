FROM node:14.15.4-alpine3.10

ARG PORT

RUN mkdir -p /usr/src/b45_cicd_express

WORKDIR /usr/src/b45_cicd_express

COPY package*.json ./
RUN npm config set package-lock false
RUN npm install
RUN npm audit fix
# RUN echo "module.exports = {PORT: $PORT}" > config.js
COPY . .

EXPOSE 8787

CMD ["npm","start"]